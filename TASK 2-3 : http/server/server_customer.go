package main

import (
	"encoding/json"
	"log"
	"net/http"
)

// specify variable with first letter capital

type customer struct {
	Id    string `json:"id"`
	First string `json:"first"`
	Last  string `json:"last"`
	Email string `json:"email"`
	Phone string `json:"phone"`
}

var customer_list []customer

func create(id, first, last, email, phone string) {

	_c := customer{Id: id, First: first, Last: last, Email: email, Phone: phone}
	customer_list = append(customer_list, _c)
}
func update(oldID, newID, first, last, email, phone string) {
	for i, v := range customer_list {
		if v.Id == oldID {
			customer_list[i].Id = newID
			customer_list[i].First = first
			customer_list[i].Last = last
			customer_list[i].Email = email
			customer_list[i].Phone = phone
		}
	}
}
func delete(id string) {

	for i, v := range customer_list {
		if v.Id == id {
			customer_list = append(customer_list[:i], customer_list[i+1:]...) //removing ith customer
			break
		}
	}
}

func createhttp(w http.ResponseWriter, r *http.Request) {

	decoder := json.NewDecoder(r.Body)
	var c customer
	err := decoder.Decode(&c)
	if err != nil {
		panic(err)
	}
	log.Println(c.Id)

	create(c.Id, c.First, c.Last, c.Email, c.Phone)
	w.Write([]byte("Customer Created"))
	log.Println("Customer created")

}
func updatehttp(w http.ResponseWriter, r *http.Request) {

	id, ok1 := r.URL.Query()["id"]
	first, ok2 := r.URL.Query()["first"]
	last, ok3 := r.URL.Query()["last"]
	email, ok4 := r.URL.Query()["email"]
	phone, ok5 := r.URL.Query()["phone"]
	oldID, ok6 := r.URL.Query()["oid"]

	if !ok1 || !ok2 || !ok3 || !ok4 || !ok5 || !ok6 {
		log.Println("Url Param 'key' is missing")
		return
	}
	update(oldID[0], id[0], first[0], last[0], email[0], phone[0])
	w.Write([]byte("Customer Updated"))
	log.Println("Customer Updated")

}
func deletehttp(w http.ResponseWriter, r *http.Request) {

	id, ok1 := r.URL.Query()["id"]

	if !ok1 {
		log.Println("Url Param 'key' is missing")
		return
	}
	delete(id[0])

	w.Write([]byte("Customer Deleted"))

	log.Println("Customer Deleted")

}
func searchhttp(w http.ResponseWriter, r *http.Request) {

	first, ok1 := r.URL.Query()["first"]

	if !ok1 {
		log.Println("Url Param 'key' is missing")
		return
	}
	var c customer
	for _, v := range customer_list {
		if v.First == first[0] {
			c = customer{Id: v.Id, First: v.First, Last: v.Last, Email: v.Email, Phone: v.Phone}
			break
		}
	}
	customerJson, err := json.Marshal(c)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(customerJson)
	log.Println("searched")

}
func listhttp(w http.ResponseWriter, r *http.Request) {
	for _, v := range customer_list {
		log.Println(v.First)
	}
	customerJson, err := json.Marshal(customer_list)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(customerJson)
	log.Println("Listed")

}

func main() {
	http.HandleFunc("/create", createhttp)
	http.HandleFunc("/update", updatehttp)
	http.HandleFunc("/delete", deletehttp)
	http.HandleFunc("/search", searchhttp)
	http.HandleFunc("/print", listhttp)

	http.ListenAndServe(":8000", nil)

}
