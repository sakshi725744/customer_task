package main

import (
	"context"
	"fmt"
	"log"
	"net"

	proto "github.com/org/repo/proto"

	"google.golang.org/grpc"
)

type server struct {
	proto.UnimplementedCustomerMethodsServer
	CustMethods proto.Customers
}

func newServer() *server {
	s := &server{}
	return s
}

func main() {
	fmt.Println("Go gRPC Beginners Tutorial!")

	listener, err := net.Listen("tcp", fmt.Sprintf(":%d", 9000))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)

	}
	srv := grpc.NewServer()

	proto.RegisterCustomerMethodsServer(srv, newServer().UnimplementedCustomerMethodsServer)

	if e := srv.Serve(listener); e != nil {
		log.Fatalf("failed to serve: %s", err)
	}

}

func (s *server) CreateCustomer(ctx context.Context, request *proto.CreateCustomerRequest) (*proto.Customers, error) {
	log.Printf("Receive message body from client: %s", request.GetCustomer())
	id, first, last, email, phone := request.GetCustomer().GetId(), request.GetCustomer().GetFirst(), request.GetCustomer().GetLast(), request.GetCustomer().GetEmail(), request.GetCustomer().GetPhone()
	s.CustMethods.Customers[id] = &proto.Customer{Id: id, First: first, Last: last, Email: email, Phone: phone}

	return &s.CustMethods, nil
}

func (s *server) UpdateCustomer(ctx context.Context, request *proto.UpdateCustomerRequest) (*proto.Customers, error) {
	id, first, last, email, phone := request.GetCustomer().GetId(), request.GetCustomer().GetFirst(), request.GetCustomer().GetLast(), request.GetCustomer().GetEmail(), request.GetCustomer().GetPhone()
	s.CustMethods.Customers[id] = &proto.Customer{Id: id, First: first, Last: last, Email: email, Phone: phone}

	return &s.CustMethods, nil
}

func (s *server) DeleteCustomer(ctx context.Context, request *proto.DeleteCustomerRequest) {
	id := request.GetId()
	delete(s.CustMethods.Customers, id)
}

func (s *server) SearchCustomer(ctx context.Context, request *proto.SearchCustomerRequest) (*proto.Customer, error) {
	email := request.GetEmail()
	var c *proto.Customer
	for _, v := range s.CustMethods.Customers {
		if v.Email == email {
			c = v
			break
		}
	}
	return c, nil
}

func (s *server) ListCustomer(ctx context.Context) (*proto.Customers, error) {
	return &s.CustMethods, nil
}
