package main

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func Test_customers_createhttp(t *testing.T) {
	tt := []struct {
		name       string
		method     string
		s          customers
		body       string
		want       string
		statusCode int
	}{
		{
			name:   "customer 1",
			method: http.MethodPost,
			s:      customers{},
			body: `{"id": "123","first": "sakshi","last": "sakshi","email": "sakshi",	"phone": "sakshi"}`,
			want:       "Customer Created",
			statusCode: http.StatusOK,
		},
		{
			name:   "customer 2",
			method: http.MethodPost,
			s:      customers{},
			body: `{"id":"157" ,"first": "sakshi","last": "sakshi","email": "sakshi",	"phone": "sakshi"}`,
			want:       "Customer Created",
			statusCode: http.StatusOK,
		},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			request := httptest.NewRequest(tc.method, "http://localhost:8000/create", strings.NewReader(tc.body))
			responseRecorder := httptest.NewRecorder()

			(tc.s).createhttp(responseRecorder, request)

			if responseRecorder.Code != tc.statusCode {
				t.Errorf("Want status '%d', got '%d'", tc.statusCode, responseRecorder.Code)
			}

			if strings.TrimSpace(responseRecorder.Body.String()) != tc.want {
				t.Errorf("Want '%s', got '%s'", tc.want, responseRecorder.Body)
			}
		})
	}
}

func Test_customers_updatehttp(t *testing.T) {
	tt := []struct {
		name       string
		method     string
		s          customers
		body       string
		want       string
		statusCode int
	}{
		{
			name:   "customer 1",
			method: http.MethodPost,
			s:      customers{},
			body: `{"id": "123","first": "sakshi","last": "sakshi","email": "sakshi",	"phone": "sakshi"}`,
			want:       "Customer Created",
			statusCode: http.StatusOK,
		},
		{
			name:   "customer 2",
			method: http.MethodPost,
			s:      customers{},
			body: `{"id":"157" ,"first": "sakshi","last": "sakshi","email": "sakshi",	"phone": "sakshi"}`,
			want:       "Customer Updated",
			statusCode: http.StatusOK,
		},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			request := httptest.NewRequest(tc.method, "http://localhost:8000/update", strings.NewReader(tc.body))
			responseRecorder := httptest.NewRecorder()

			(tc.s).updatehttp(responseRecorder, request)

			if responseRecorder.Code != tc.statusCode {
				t.Errorf("Want status '%d', got '%d'", tc.statusCode, responseRecorder.Code)
			}

			if strings.TrimSpace(responseRecorder.Body.String()) != tc.want {
				t.Errorf("Want '%s', got '%s'", tc.want, responseRecorder.Body)
			}
		})
	}
}

func Test_customers_deletehttp(t *testing.T) {
	tt := []struct {
		name       string
		method     string
		s          customers
		body       string
		want       string
		statusCode int
	}{
		{
			name:       "customer 1",
			method:     http.MethodDelete,
			s:          customers{"123": customer{Id: "123", First: "sakshi", Last: "sakshi", Email: "sakshi", Phone: "sakshi"}, "234": customer{Id: "234", First: "parikh", Last: "Parikh", Email: "sakshi", Phone: "sakshi"}},
			body:       `{"id": "234"}`,
			want:       "Customer Deleted",
			statusCode: http.StatusOK,
		},
		{
			name:   "customer 2",
			method: http.MethodPost,
			s:      customers{"123": customer{Id: "123", First: "sakshi", Last: "sakshi", Email: "sakshi", Phone: "sakshi"}, "234": customer{Id: "234", First: "parikh", Last: "Parikh", Email: "sakshi", Phone: "sakshi"}},
			body:   `{"id":"157"}`,
			want: `{"id": "123","first": "sakshi","last": "sakshi","email": "sakshi",	"phone": "sakshi"}`,
			statusCode: http.StatusOK,
		},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			request := httptest.NewRequest(tc.method, "http://localhost:8000/delete", strings.NewReader(tc.body))
			responseRecorder := httptest.NewRecorder()

			(tc.s).deletehttp(responseRecorder, request)

			if responseRecorder.Code != tc.statusCode {
				t.Errorf("Want status '%d', got '%d'", tc.statusCode, responseRecorder.Code)
			}

			if strings.TrimSpace(responseRecorder.Body.String()) != tc.want {
				t.Errorf("Want '%s', got '%s'", tc.want, responseRecorder.Body)
			}
		})
	}
}

func Test_customers_searchhttp(t *testing.T) {
	tt := []struct {
		name       string
		method     string
		s          customers
		body       string
		want       string
		statusCode int
	}{
		{
			name:       "customer 1",
			method:     http.MethodGet,
			s:          customers{"123": customer{Id: "123", First: "sakshi", Last: "sakshi", Email: "sakshi@appointy", Phone: "sakshi"}, "234": customer{Id: "234", First: "parikh", Last: "Parikh", Email: "parikh@appointy", Phone: "sakshi"}},
			body:       "sakshi",
			want:       `[{"id":"123","first":"sakshi","last":"sakshi","email":"sakshi@appointy","phone":"sakshi"}]`,
			statusCode: http.StatusOK,
		},
		{
			name:       "customer 2",
			method:     http.MethodGet,
			s:          customers{"123": customer{Id: "123", First: "sakshi", Last: "sakshi", Email: "sakshi@appointy", Phone: "sakshi"}, "234": customer{Id: "234", First: "parikh", Last: "parikh", Email: "parikh@appointy", Phone: "sakshi"}},
			body:       "sakshi",
			want:       `[{"id":"234","first":"parikh","last":"parikh","email":"parikh@appointy","phone":"sakshi"}]`,
			statusCode: http.StatusOK,
		},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			request := httptest.NewRequest(tc.method, fmt.Sprintf("http://localhost:8000/search?first=%v", tc.body), nil)
			responseRecorder := httptest.NewRecorder()

			(tc.s).searchhttp(responseRecorder, request)

			if responseRecorder.Code != tc.statusCode {
				t.Errorf("Want status '%d', got '%d'", tc.statusCode, responseRecorder.Code)
			}

			if strings.TrimSpace(responseRecorder.Body.String()) != tc.want {
				t.Errorf("Want '%s', got '%s'", tc.want, responseRecorder.Body)
			}
		})
	}
}

func Test_customers_listhttp(t *testing.T) {
	tt := []struct {
		name       string
		method     string
		s          customers
		want       string
		statusCode int
	}{
		{
			name:       "customer 1",
			method:     http.MethodGet,
			s:          customers{"123": customer{Id: "123", First: "sakshi", Last: "sakshi", Email: "sakshi", Phone: "sakshi"}},
			want:       `[{"id":"123","first":"sakshi","last":"sakshi","email":"sakshi","phone":"sakshi"}]`,
			statusCode: http.StatusOK,
		},
		{
			name:       "customer 2",
			method:     http.MethodPost,
			s:          customers{"234": customer{Id: "234", First: "parikh", Last: "Parikh", Email: "sakshi", Phone: "sakshi"}},
			want:       `[{"id": "123","first":"sakshi","last":"sakshi","email": "sakshi","phone":"sakshi"}]`,
			statusCode: http.StatusOK,
		},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			request := httptest.NewRequest(tc.method, "http://localhost:8000/print", nil)
			responseRecorder := httptest.NewRecorder()

			(tc.s).listhttp(responseRecorder, request)

			if responseRecorder.Code != tc.statusCode {
				t.Errorf("Want status '%d', got '%d'", tc.statusCode, responseRecorder.Code)
			}

			if strings.TrimSpace(responseRecorder.Body.String()) != tc.want {
				t.Errorf("Want '%s', got '%s'", tc.want, responseRecorder.Body)
			}
		})
	}
}
