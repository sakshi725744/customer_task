package main

import (
	"context"
	"log"
	"time"

	proto "github.com/org/repo/proto"
	"google.golang.org/grpc"
)

func main() {
	conn, err := grpc.Dial(":9000", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := proto.NewCustomerMethodsClient(conn)

	// Contact the server and print out its response.

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	r, err := c.CreateCustomer(ctx, &proto.CreateCustomerRequest{Customer: &proto.Customer{Id: "ab", First: "Sakshi", Email: "123", Last: "par", Phone: "12345"}})
	if err != nil {
		log.Fatalf("could not greet: %v", err)
	}
	log.Printf("Greeting: %s", r.GetCustomers())
}
