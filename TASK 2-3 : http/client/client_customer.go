package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
)

type customer struct {
	Id    string `json:"id"`
	First string `json:"first"`
	Last  string `json:"last"`
	Email string `json:"email"`
	Phone string `json:"phone"`
}

func createResp(id, first, last, email, phone string) {
	postBody, _ := json.Marshal(map[string]string{
		"id":    id,
		"first": first,
		"last":  last,
		"email": email,
		"phone": phone,
	})
	responseBody := bytes.NewBuffer(postBody)
	resp, err := http.Post("http://localhost:8000/create", "application/json", responseBody)
	// URL-encoded payload

	if err != nil {
		log.Fatalln(err)
	}
	defer resp.Body.Close()
	//We Read the response body on the line below.
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}
	//Convert the body to type string
	sb := string(body)
	log.Printf(sb)
}

func updateResp(oid, nid, first, last, email, phone string) {

	resp, err := http.PostForm("http://localhost:8000/update", url.Values{"oid": {oid}, "nid": {nid}, "first": {first}, "last": {last}, "email": {email}, "phone": {phone}})
	if err != nil {
		log.Fatalln(err)
	}
	//We Read the response body on the line below.
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}
	//Convert the body to type string
	sb := string(body)
	log.Printf(sb)
}

func deleteResp(id string) {

	resp, err := http.PostForm("http://localhost:8000/delete", url.Values{"id": {id}})
	if err != nil {
		log.Fatalln(err)
	}
	//We Read the response body on the line below.
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}
	//Convert the body to type string
	sb := string(body)
	log.Printf(sb)
}
func listResp() {

	resp, err := http.Get("http://localhost:8000/print")
	if err != nil {
		log.Fatalln(err)
	}
	//We Read the response body on the line below.
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}
	//Convert the body to type string
	sb := string(body)
	log.Printf(sb)
}
func searchResp(email string) {

	resp, err := http.Get(fmt.Sprintf("http://localhost:8000/search?first=%v", email))
	if err != nil {
		log.Fatalln(err)
	}
	//We Read the response body on the line below.
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}
	//Convert the body to type string
	sb := string(body)
	log.Printf(sb)
}

func main() {
	var _a string
	_t := true
	for _t == true {
		fmt.Println("Enter C to create")
		fmt.Println("Enter U to Update")
		fmt.Println("Enter D to Delete")
		fmt.Println("Enter P to list customers")
		fmt.Println("Enter S to list customers with name")
		fmt.Println("Enter Q to Quit")
		fmt.Print("Enter character: ")
		fmt.Scanln(&_a)

		var _c1 customer
		switch _a {
		case "C":
			{
				fmt.Print("Enter customer id: ")
				fmt.Scanln(&_c1.Id)
				fmt.Print("Enter customer first name: ")
				fmt.Scanln(&_c1.First)
				fmt.Print("Enter customer last name: ")
				fmt.Scanln(&_c1.Last)
				fmt.Print("Enter customer email: ")
				fmt.Scanln(&_c1.Email)
				fmt.Print("Enter customer phone: ")
				fmt.Scanln(&_c1.Phone)
				createResp(_c1.Id, _c1.First, _c1.Last, _c1.Email, _c1.Phone)
				fmt.Println("------------------------------------")

			}
		case "U":
			{
				var oid string
				fmt.Print("Enter old customer id: ")
				fmt.Scanln(&oid)
				fmt.Print("Enter updated customer id: ")
				fmt.Scanln(&_c1.Id)
				fmt.Print("Enter updated customer first name: ")
				fmt.Scanln(&_c1.First)
				fmt.Print("Enter updated customer last name: ")
				fmt.Scanln(&_c1.Last)
				fmt.Print("Enter updated customer email: ")
				fmt.Scanln(&_c1.Email)
				fmt.Print("Enter updated customer phone: ")
				fmt.Scanln(&_c1.Phone)
				updateResp(oid, _c1.Id, _c1.First, _c1.Last, _c1.Email, _c1.Phone)
				fmt.Println("------------------------------------")

			}
		case "D":
			{
				var _id string
				fmt.Print("Enter customer id for deletion: ")
				fmt.Scanln(&_id)
				deleteResp(_id)
				fmt.Println("------------------------------------")

			}
		case "P":
			{
				listResp()
				fmt.Println("------------------------------------")

			}
		case "Q":
			{
				_t = false
				fmt.Println("------------------------------------")

			}
		case "S":
			{
				var _first string
				fmt.Print("Enter customer first name to search: ")
				fmt.Scanln(&_first)
				searchResp(_first)
				fmt.Println("------------------------------------")

			}
		default:
			{
				fmt.Println("INCORRECT CHARACTER")
				fmt.Println("------------------------------------")

			}
		}

	}
}
