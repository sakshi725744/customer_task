package main

import (
	"encoding/json"
	"log"
	"net/http"
)

// specify variable with first letter capital

type customer struct {
	Id    string `json:"id"`
	First string `json:"first"`
	Last  string `json:"last"`
	Email string `json:"email"`
	Phone string `json:"phone"`
}

type customers map[string]customer

type custHttpMethods interface {
	createhttp(w http.ResponseWriter, r *http.Request)
	updatehttp(w http.ResponseWriter, r *http.Request)
	deletehttp(w http.ResponseWriter, r *http.Request)
	searchhttp(w http.ResponseWriter, r *http.Request)
	listhttp(w http.ResponseWriter, r *http.Request)
}

func NewCustomers() custHttpMethods {
	return customers{}
}

func (s customers) create(c customer) {
	s[c.Id] = c
}

func (s customers) update(c customer) {
	s[c.Id] = c
}

func (s customers) del(id string) {
	delete(s, id)
}
func (s customers) search(first string) []customer {
	var result []customer
	for _, v := range s {
		if v.First == first {
			result = append(result, v)
		}
	}
	return result
}
func (s customers) list() []customer {
	var result []customer
	for _, v := range s {

		result = append(result, v)

	}
	return result
}

func (s customers) createhttp(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		decoder := json.NewDecoder(r.Body)
		var c customer
		err := decoder.Decode(&c)
		if err != nil {
			panic(err)
		}

		s.create(c)
		w.Write([]byte("Customer Created"))
		log.Println("Customer Created")
		return
	}
	w.Write([]byte("only POST requests permitted"))
}
func (s customers) updatehttp(w http.ResponseWriter, r *http.Request) {

	if r.Method == "POST" {
		decoder := json.NewDecoder(r.Body)
		var c customer
		err := decoder.Decode(&c)
		if err != nil {
			panic(err)
		}

		s.update(c)
		w.Write([]byte("Customer Updated"))
		log.Println("Customer Updated")
		return
	}
	w.Write([]byte("only POST requests permitted"))

}
func (s customers) deletehttp(w http.ResponseWriter, r *http.Request) {
	if r.Method == "DELETE" {
		type cust struct {
			Id string `json:"id"`
		}
		var c cust
		decoder := json.NewDecoder(r.Body)
		err := decoder.Decode(&c)
		if err != nil {
			panic(err)
		}
		log.Print(c.Id)
		s.del(c.Id)
		w.Write([]byte("Customer Deleted"))

		log.Println("Customer Deleted")
		return
	}
	w.Write([]byte("only DELETE requests permitted"))

}
func (s customers) searchhttp(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		first, ok1 := r.URL.Query()["first"]

		if !ok1 {
			log.Println("Url Param 'key' is missing")
			return
		}

		customerJson, err := json.Marshal(s.search(first[0]))
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.Write(customerJson)
		log.Println("searched")
		return
	}
	w.Write([]byte("only GET requests permitted"))

}
func (s customers) listhttp(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		customerJson, err := json.Marshal(s.list())
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.Write(customerJson)
		log.Println("Listed")
		return
	}
	w.Write([]byte("only GET requests permitted"))

}
func httpCustomer(s custHttpMethods) {

	http.HandleFunc("/create", s.createhttp)
	http.HandleFunc("/update", s.updatehttp)
	http.HandleFunc("/delete", s.deletehttp)
	http.HandleFunc("/search", s.searchhttp)
	http.HandleFunc("/print", s.listhttp)

	http.ListenAndServe(":8000", nil)
}

func main() {
	c := NewCustomers()
	httpCustomer(c)

}
